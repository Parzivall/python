import numpy as np
import matplotlib.pyplot as plt
from skimage import io, img_as_ubyte
from skimage.color import rgb2gray
import cv2
import random

def show2imgs(im1, im2, title1='Obraz pierwszy', title2='Obraz drugi', size=(10,10)):

    f, (ax1, ax2) = plt.subplots(1,2, figsize=size)
    ax1.imshow(im1, cmap='gray')
    ax1.axis('off')
    ax1.set_title(title1)

    ax2.imshow(im2, cmap='gray')
    ax2.axis('off')
    ax2.set_title(title2)
    plt.show()
    
def segmentuj(img):
    for i in range(img.shape[0]):
        for j in range(img.shape[1]):
            if img[i][j] != 255:
                if img[i][j] == 0:
                    img[i][j] = random.randint(1, 100)
                    if img[i-1][j-1] != 0 and img[i-1][j-1] != 255:
                        img[i][j] = img[i-1][j-1]
                    elif img[i-1][j] != 0 and img[i-1][j] != 255:
                        img[i][j] = img[i-1][j]
                    elif img[i-1][j+1] != 0 and img[i-1][j+1] != 255:
                        img[i][j] = img[i-1][j+1]
                    elif img[i][j-1] != 0 and img[i][j-1] != 255:
                        img[i][j] = img[i][j-1]
                    elif img[i][j+1] != 0 and img[i][j+1] != 255:
                        img[i][j] = img[i][j+1]
                    elif img[i+1][j-1] != 0 and img[i+1][j-1] != 255:
                        img[i][j] = img[i-1][j-1]
                    elif img[i+1][j] != 0 and img[i+1][j] != 255:
                        img[i][j] = img[i+1][j]
                    elif img[i+1][j+1] != 0 and img[i+1][j+1] != 255:
                        img[i][j] = img[i+1][j+1]
                    else:
                        continue
                    #scalanie obiektów
                if img[i][j] != img[i-1][j-1] and img[i-1][j-1] != 255:
                    img[i][j] = img[i-1][j-1]
                    
                elif img[i][j] != img[i-1][j] and img[i-1][j] != 255:
                    img[i][j] = img[i-1][j]
                    
                elif img[i][j] != img[i-1][j+1] and img[i-1][j+1] != 255:
                    img[i-1][j+1] = img[i][j]
                    
                elif img[i][j] != img[i][j-1] and img[i][j-1] != 255:
                    img[i][j-1] = img[i][j]
                    
                elif img[i][j] != img[i][j+1] and img[i][j+1] != 255:
                    img[i][j+1] = img[i][j]
                    
                elif img[i][j] != img[i+1][j-1] and img[i+1][j-1] != 255:
                    img[i][j] = img[i+1][j-1]
                    
                elif img[i][j] != img[i+1][j] and img[i+1][j] != 255:
                    img[i][j] = img[i+1][j]
                elif img[i][j] != img[i+1][j+1] and img[i+1][j+1] != 255:
                    img[i][j] = img[i+1][j+1]
                else:
                    continue
                    
                #if img[i][j] != img[i-1][j-1] and img[i-1][j-1] != 255:
                #    img[i][j] = img[i-1][j-1]
                #elif img[i][j] != img[i-1][j] and img[i-1][j] != 255:
                #    img[i][j] = img[i-1][j]
                #elif img[i][j] != img[i-1][j+1] and img[i-1][j+1] != 255:
                #    img[i][j] = img[i-1][j+1]
                #elif img[i][j] != img[i][j-1] and img[i][j-1] != 255:
                #    img[i][j] = img[i][j-1]
                #elif img[i][j] != img[i][j+1] and img[i][j+1] != 255:
                #    img[i][j] = img[i][j+1]
                #elif img[i][j] != img[i+1][j-1] and img[i+1][j-1] != 255:
                #    img[i][j] = img[i+1][j-1]
                #elif img[i][j] != img[i+1][j] and img[i+1][j] != 255:
                #    img[i][j] = img[i+1][j]
                #elif img[i][j] != img[i+1][j+1] and img[i+1][j+1] != 255:
                #    img[i][j] = img[i+1][j+1]
                #else:
                #    continue
    return img

def scal(img):
    for i in range(img.shape[0]):
        for j in range(img.shape[1]):
            if img[i][j] != 255:               
                if img[i][j] != img[i-1][j-1] and img[i-1][j-1] != 255:
                    img[i][j] = img[i-1][j-1]
                    
                elif img[i][j] != img[i-1][j] and img[i-1][j] != 255:
                    img[i][j] = img[i-1][j]
                    
                elif img[i][j] != img[i-1][j+1] and img[i-1][j+1] != 255:
                    img[i-1][j+1] = img[i][j]
                    
                elif img[i][j] != img[i][j-1] and img[i][j-1] != 255:
                    img[i][j-1] = img[i][j]
                    
                elif img[i][j] != img[i][j+1] and img[i][j+1] != 255:
                    img[i][j+1] = img[i][j]
                    
                elif img[i][j] != img[i+1][j-1] and img[i+1][j-1] != 255:
                    img[i][j] = img[i+1][j-1]
                    
                elif img[i][j] != img[i+1][j] and img[i+1][j] != 255:
                    img[i][j] = img[i+1][j]
                elif img[i][j] != img[i+1][j+1] and img[i+1][j+1] != 255:
                    img[i][j] = img[i+1][j+1]
                else:
                    continue
    return img


def licz_obiekty(img):
    ile_obiektow = 0
    obiekty = []
    for i in range(img.shape[0]):
        for j in range(img.shape[1]):
            if img[i][j] != 255:
                obiekty.append(img[i][j])
    ile_obiektow = len(list(set(obiekty)))# zlicza niepowtarzające się elementy listy
    return ile_obiektow
    
img = io.imread('pattern1.png')
th = 110
img = img_as_ubyte(rgb2gray(img))
th, bim = cv2.threshold(img, thresh=th, maxval=255, type=cv2.THRESH_BINARY)
bimCPY = bim;
plt.imshow(bimCPY, cmap="gray")
plt.axis('on')
plt.show()


#najlepiej uruchomić segmentację 2 razy gdyż przez "niewielki" zakres możliwych kolorów mogą się one powtarzać

img = segmentuj(bim)
nimg = scal(img)
nimg = scal(nimg)
plt.imshow(nimg, cmap="hot")
plt.axis('on')
plt.show()
    
ile = licz_obiekty(nimg)
print("Obiektów jest:\t", ile)
            